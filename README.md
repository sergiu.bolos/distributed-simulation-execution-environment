# Client-Server Simulation Execution Environment

## Description

This project involves the development of a task management system that uses a server-client architecture to handle and process tasks. The tasks are uploaded by users, processed by workers, and the results are stored and made available for download. The entire source code can be found in the repository linked below.

## Repository

The entire source code of this project is hosted on GitLab. You can access it using the following link:

[GitLab Repository](https://gitlab.upt.ro/sergiu.bolos/distributed-simulation-execution-environment)

## Deliverables

1. **Source Code**: The repository contains the complete source code of the project without the compiled binaries.
2. **Documentation**: This README file includes instructions for building, installing, and launching the application.

## Application Build Steps

To build the application, follow these steps:

1. **Clone the Repository**:
    ```bash
    git clone https://gitlab.upt.ro/sergiu.bolos/distributed-simulation-execution-environment
    cd your-repo-directory
    ```

2. **Install Dependencies**:
    Make sure you have Python installed. Then, install the required Python packages:
    ```bash
    pip install -r requirements.txt
    ```

3. **Set Up MongoDB**:
    Ensure MongoDB is installed and running. Update the MongoDB connection string in `Config.py` if necessary.

## Application Installation and Launch Steps

To install and launch the application, follow these steps:

1. **Server and Worker Setup**:
    - Navigate to the server directory:
        ```bash
        cd path/to/server/directory
        ```
    - Run the server (which also starts the worker process):
        ```bash
        python Server.py
        ```
    This will start the Flask server which handles task submissions, status queries, and it will also start the worker process for handling task processing. Additionally, this will also start the worker.

2. **Client Usage**:
    - To interact with the system, the user has 2 options: using commands via the CLI or access the GUI
      - CLI example usage:
          ```bash
          python MainApp.py --add_task --data_path /absolute/path/to/your/data.zip --version 1.0
          python MainApp.py --status --status_task_id <task_id>
          python MainApp.py --download --download_task_id <task_id> --save_path /path/to/save/directory
          ```
**Note:** For the whole setup to function properly, you need the be connected to the company's VPN.
## Application Components

### Main Files

- `Server.py`: Contains the Flask server for handling HTTP requests and starts the worker process.
- `Worker.py`: Manages task processing using a worker thread.
- `MainApp.py`: CLI for interacting with the task management system.
- `UserInterface.py`: Main window of the GUI.
- `TasksTab.py`: Tab inside the GUI for managing tasks.
- `ConnectionHandler.py`: Handles connections between client and server.
- `SimulationRunner.py`: Runs simulations for the tasks.
- `Config.py`: Configuration file containing database connection strings and other settings.
- `Utils.py`: Utility functions for the application.
- `ClientThread.py`: Manages client threads.
- `ArtifactoryUtilities.py`: Utilities for handling Artifactory operations.
- `SimulationUtilities.py`: Contains utility functions for simulations.

### Additional Directories

- `static/files`: Directory for storing uploaded and processed files.

### Requirements

- Python 3.x
- MongoDB
- Flask
- PyQt5
- Additional Python packages listed in `requirements.txt`.

## Contact

For any queries or support, please contact me at [sergiu.bolos@student.upt.ro](sergiu.bolos@student.upt.ro).

