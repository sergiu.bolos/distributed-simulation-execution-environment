from datetime import datetime


class Task:
    def __init__(self, start_time, end_time, user, data_path, result_path, version, size, status):
        self.start_time = start_time
        self.end_time = end_time
        self.user = user
        self.data_path = data_path  # path to zip containing all data
        self.result_path = result_path  # result path where the rdb will be outputted
        self.version = version  # tool version
        self.size = size  # task size
        self.status = status  # new/processing/done

    def set_start_time(self):
        self.start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
