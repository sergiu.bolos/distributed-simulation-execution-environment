import os
from pymongo import MongoClient

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
UP_FOLDER = 'static\\files'
RECEIVED_FILES_DIR = os.path.join(ROOT_PATH, UP_FOLDER)
MAX_STORAGE_CAPACITY_GB = 30

# instance to the MongoDB client
mongo_client = MongoClient('mongodb://localhost:12345/')

# collection setup
db1 = mongo_client['database_name']
task_collection1 = db1['collection_name']
