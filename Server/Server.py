import multiprocessing
import os
import subprocess
import uuid

from bson import ObjectId
from flask import Flask, jsonify, request, send_file
from werkzeug.utils import secure_filename

rom Task import Task
from Config import task_collection1, RECEIVED_FILES_DIR
from Utils import create_zip_from_result

app = Flask(__name__)

# secret key for the app
app.config['UPLOAD_FOLDER'] = 'static\\files'


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'zip'}


# define routes for TaskQueue methods
task_queue = []


# route for fetching server status
@app.route('/status', methods=['GET'])
def is_on():
    return jsonify({"status": "ok"}), 200


# route for uploading the zip to the server and adding the task in the db
@app.route('/tasks', methods=['POST'])
def add_task():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part!'}), 400

    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No selected file!'}), 400

    if not allowed_file(file.filename):
        return jsonify({'error': 'Invalid file format!'}), 400

    task_data = request.form.to_dict()
    required_fields = ['user', 'version']
    if not all(field in task_data for field in required_fields):
        return jsonify({'error': 'Missing required fields in the request.'}), 400

    # insert task in MongoDB
    try:

        # file upload part
        file_name = secure_filename(file.filename)
        joined_file_path = os.path.join(RECEIVED_FILES_DIR, f"{str(uuid.uuid4())}_{file_name}")

        if not os.path.exists(os.path.dirname(joined_file_path)):
            os.makedirs(os.path.dirname(joined_file_path))

        # noinspection PyTypeChecker
        file.save(joined_file_path)

        # task addition part
        new_task = Task(
            start_time=task_data['start_time'],
            end_time=task_data['end_time'],
            user=task_data['user'],
            data_path=joined_file_path,
            result_path=task_data['result_path'],
            version=task_data['version'],
            size=task_data['size'],
            status=task_data['status']
        )

        task_queue.append(new_task)
        task_id = task_collection1.insert_one(new_task.__dict__).inserted_id

        return jsonify({'message': f'Task {str(task_id)} was added successfully.', 'task_id': str(task_id)}), 201

    except Exception as e:
        print(f"Error inserting task into MongoDB: {str(e)}")
        return jsonify({'error': 'An error occurred while adding the task.'}), 500


# route for retrieving task queue from the Server
# if the request has an user ID as a parameter, it will send a list of task IDs
# else it will send a data regarding the task size and status
@app.route('/tasks', methods=['GET'])
def get_task_queue():
    task_queue_data = []

    user_id = request.args.get('user_id')

    if user_id:
        # query server for tasks associated with the user id
        tasks = task_collection1.find({"user": user_id})
        for task in tasks:
            task_queue_data.append(str(task['_id']))

    else:
        tasks = task_collection1.find({"status": {"$in": ["new", "processing"]}})

        # convert MongoDB documents to a list of dictionaries
        for task in tasks:
            task_queue_data.append({
                'size': task['size'],
                'status': task['status'],
            })

    return jsonify({'task_queue': task_queue_data})


# route for fetching the status, and eventually the end time of one specific task based on its ID
@app.route('/task', methods=['GET'])
def get_task_status():
    task_id = request.args.get('task_id')

    if task_id:
        try:
            task = task_collection1.find_one({"_id": ObjectId(task_id)})
            if task:
                status = task.get('status')
                end_time = task.get('end_time')
                if status is not None and end_time is not None:
                    return jsonify({
                        'size': task.get('size'),
                        'status': status,
                        'start_time': task.get('start_time'),
                        'end_time': end_time,
                        'server': task.get('server'),
                    }), 200
                else:
                    return jsonify({'error': 'Status field is not set for the task!'}), 500
            else:
                return jsonify({'error': 'Task not found!'}), 404
        except Exception as e:
            return jsonify({'error': str(e)}), 500
    else:
        return jsonify({'error': 'Task ID not provided in the query parameters!'}), 400


# route for downloading file associated with a task
@app.route('/files', methods=['GET'])
def download_file():
    task_id = request.args.get('task_id')
    if task_id:
        task = task_collection1.find_one({'_id': ObjectId(task_id)})
        if task:
            task['_id'] = str(task['_id'])
            result_path = task.get('result_path')
            if result_path and os.path.exists(result_path):
                # create and send zip file
                zip_file_path = create_zip_from_result(result_path)
                return send_file(zip_file_path, as_attachment=True)
            else:
                return jsonify({'error': 'Result path not found or invalid!'}), 500
        else:
            return jsonify({'error': 'Task not found!'}), 404
    else:
        return jsonify({'error': 'Task ID not provided in the query parameters!'}), 400


def start_worker_script():
    subprocess.Popen(["cmd", "/k", "start", "cmd", "/k", "python Worker.py"], shell=True)


if __name__ == '__main__':
    worker_process = multiprocessing.Process(target=start_worker_script)
    worker_process.start()

    app.run(debug=True, port=5001)
    worker_process.join()
