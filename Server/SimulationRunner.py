import os.path
import re
import subprocess
import zipfile

from pathlib import Path
from Utils import TOOL_VERSIONS_DIRECTORY
from Config import RECEIVED_FILES_DIR


class SimulationRunner:

    def __init__(self, results_directory):
        self.results_directory = results_directory

    def create_results_dir(self, param_path):
        self.results_directory = os.path.join(os.path.dirname(param_path), 'results')
        if not os.path.isdir(self.results_directory):
            os.makedirs(self.results_directory)

        with open(param_path, 'r') as par_file:
            lines = par_file.readlines()

        for i, line in enumerate(lines):
            if 'LogFileFolder=' in line:
                field, value = line.split('=')
                lines[i] = f'{field}={self.results_directory}\n'

        with open(param_path, 'w') as par_file:
            par_file.writelines(lines)

        return self.results_directory

    def search_for_rdb(self):
        rdb_regex = re.compile('(.*rdb$)')
        for file_in_results in os.listdir(self.results_directory):
            if rdb_regex.match(file_in_results):
                return True

        return False

    # function to build path to the exe
    @staticmethod
    def build_exe_path(version):
        path_to_exe = os.path.join(TOOL_VERSIONS_DIRECTORY, version)

        if 'STools.exe' in os.listdir(path_to_exe):
            path_to_exe = os.path.join(path_to_exe, 'STools.exe')
            return path_to_exe
        else:
            print(f"STools.exe not found for version {version}.")
            return None  # treat case accordingly

    @staticmethod
    def build_param_path(path):
        par_regex = re.compile('(.*par$)')
        path_to_param = os.path.join(RECEIVED_FILES_DIR, Path(path).stem)

        if not os.path.isdir(path_to_param):
            raise FileNotFoundError(f"Directory does not exist: {path_to_param}")

        for file in os.listdir(path_to_param):
            if par_regex.match(file):
                path_to_param += f'\\{file}'
                return path_to_param

        raise FileNotFoundError("Parameter file not found")

    @staticmethod
    def open_and_extract_zip(path, task_id):
        unique_path = f"{str(task_id)}_{Path(path).stem}"
        dest_dir = os.path.join(RECEIVED_FILES_DIR, unique_path)

        if not os.path.isdir(dest_dir):
            os.makedirs(dest_dir)

            with zipfile.ZipFile(path, 'r') as zip_ref:
                zip_ref.extractall(dest_dir)

            # remove zip after extraction
            os.remove(path)
        return dest_dir

    @staticmethod
    # this functions starts the tool and waits for the current task to be finished
    def run_stools_sim(exe_path, parameter_path):
        process = subprocess.Popen([exe_path, parameter_path, '-StartAnalysis', '-ShutdownAtAnalysisEnd'])
        process.wait()

    def start(self, task):
        extracted_dir = self.open_and_extract_zip(task['data_path'], task['_id'])
        param_path = self.build_param_path(extracted_dir)
        self.results_directory = self.create_results_dir(param_path)
        self.run_stools_sim(self.build_exe_path(task['version']), param_path)
        return self.search_for_rdb()
