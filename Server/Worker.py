import concurrent.futures
import os
import time
from datetime import datetime

from PyQt5.QtCore import QThread

from Config import task_collection1
from SimulationRunner import SimulationRunner
from Utils import create_tool_dir, download_tool


class Worker(QThread):
    def __init__(self, task_collection):
        super().__init__()
        self.task_collection = task_collection
        self.current_threads = 0

    # start simulation
    def start_simulation(self, task):
        task_id = str(task['_id'])

        print(f"Simulation {task_id} is starting.")
        simulation_runner = SimulationRunner(results_directory=None)
        finished_check = simulation_runner.start(task)
        if finished_check:
            end_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.task_collection.update_one({'_id': task['_id']}, {'$set': {'status': 'done', 'end_time': end_time}})
            self.task_collection.update_one({'_id': task['_id']},
                                            {'$set': {'result_path': simulation_runner.results_directory}})
        return task_id

    # thread is done, insert in mongo
    @staticmethod
    def task_done_callback(future):
        task_id = future.result()
        print(f"Callback: Simulation {task_id} is done.")

    # get oldest sim
    def get_next_task(self):
        oldest_task = self.task_collection.find_one({'status': 'new'}, sort=[('_id', 1)])

        if oldest_task:
            # check for version
            version = oldest_task.get('version')
            if version:
                tool_dir = create_tool_dir(version)
                if not os.listdir(tool_dir):
                    download_tool(version)

            print(f"Found new task. ID: {oldest_task['_id']}")
            self.task_collection.update_one({'_id': oldest_task['_id']}, {'$set': {'status': 'processing'}})
            return oldest_task

        return None

    def check_availability(self, task):
        print(f"Checking thread availability...")
        print(f"current threads: {self.current_threads}")
        threads_req = self.threads_needed(task)
        if threads_req + self.current_threads <= 4:
            return True
        return False

    @staticmethod
    def threads_needed(task):
        task_size = task.get('size', 'S')
        if task_size == 'L':
            return 4
        elif task_size == 'M':
            return 2
        return 1

    def run(self):
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            futures = []
            task_map = {}

            try:
                while True:
                    print(f"Active threads: {self.current_threads}")

                    if self.current_threads < 4:
                        task = self.get_next_task()
                        if task and self.check_availability(task):
                            print(f"Submitting simulation {task['_id']}")
                            future = executor.submit(self.start_simulation, task)
                            future.add_done_callback(self.task_done_callback)
                            futures.append(future)

                            print(f"{self.current_threads} = {self.current_threads} + {self.threads_needed(task)}")
                            self.current_threads += self.threads_needed(task)
                            task_map[future] = task['_id']
                        else:
                            print("No new tasks found or no availability. Waiting...")
                            time.sleep(5)
                    else:
                        print("All workers are busy.")
                        time.sleep(5)

                    # update current_threads count based on completed futures
                    done_futures = [f for f in futures if f.done()]
                    for df in done_futures:
                        task_id = task_map[df]
                        task = self.task_collection.find_one({'_id': task_id})
                        if task:
                            print(f"{self.current_threads} = {self.current_threads} - {self.threads_needed(task)}")
                            self.current_threads -= self.threads_needed(task)
                        futures.remove(df)
                        del task_map[df]

            except KeyboardInterrupt:
                print("Stopping task addition")

            for future in concurrent.futures.as_completed(futures):
                future.result()


if __name__ == '__main__':
    worker = Worker(task_collection=task_collection1)
    worker.start()
    worker.wait()
