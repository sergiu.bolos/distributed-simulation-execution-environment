import os.path
import shutil
import zipfile

from artifactory import ArtifactoryPath

ARTIFACTORY_PATH = ArtifactoryPath("https://eu.artifactory.conti.de/artifactory/pss_algo_tools_generic_l/bin/tools"
                                   "/stools/releases")
TOOL_VERSIONS_DIRECTORY = "D:\\ToolVersions\\"


# reset processing tasks to new if server restarts
def check_for_interrupted_tasks(task_collection):
    processing_task = task_collection.find_one({'status': 'processing'}, sort=[('_id', -1)])
    if processing_task:
        print(f"Found processing task. ID: {processing_task['_id']}")

        # change status in new
        task_collection.update_one({'_id': processing_task['_id']}, {'$set': {'status': 'new'}})
        print("Interrupted task updated...")


def create_zip_from_result(results_dir):
    # check if directory exists
    if not os.path.exists(results_dir):
        return

    # extract name from the path
    results_dir_name = os.path.basename(results_dir)

    # generate zip file
    zip_file_name = f"{results_dir_name}.zip"

    # Create the full path for the zip file
    zip_file_path = os.path.join(results_dir, zip_file_name)

    with zipfile.ZipFile(zip_file_path, 'w') as zipf:
        # walk through the directory
        for dir_name, _, files in os.walk(results_dir):
            for file in files:
                file_path = os.path.join(dir_name, file)
                if file_path != zip_file_path:
                    arc_name = os.path.relpath(file_path, results_dir)
                    zipf.write(file_path, arc_name)

    return zip_file_path


def download_tool(version):
    tool_dir = create_tool_dir(version)

    # check if version exists
    if os.listdir(tool_dir):
        return f"Version {version} already exists on the machine."

    for path in ARTIFACTORY_PATH:
        if version in str(path):
            for tool_zip in path:
                zip_name = os.path.basename(tool_zip)
                path = ArtifactoryPath(f"{ARTIFACTORY_PATH}/{version}/{zip_name}")

                with path.open() as fd:
                    with open(f"{tool_dir}\\{zip_name}", "wb") as out:
                        out.write(fd.read())

                with zipfile.ZipFile(f"{tool_dir}\\{zip_name}", 'r') as zip_ref:
                    zip_ref.extractall(tool_dir)
                return f"Version {version} downloaded successfully."

    return f"Version {version} not found in Artifactory path."


def create_tool_dir(version=""):
    tool_version_directory = os.path.join(TOOL_VERSIONS_DIRECTORY, version)

    if not os.path.exists(tool_version_directory):
        os.makedirs(tool_version_directory)

    return os.path.abspath(tool_version_directory)


# this function retrieves the size of a directory
def get_directory_size(directory):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            file_path = os.path.join(dirpath, filename)
            if os.path.exists(file_path):
                total_size += os.path.getsize(file_path)
    size_in_gb = total_size / (1024 ** 3)
    return round(size_in_gb, 2)


# this function deletes the oldest folders from a path until a desired threshold is met
def clean_directory(directory, max_size_gb):
    folders = []
    for dirpath, dirnames, filenames in os.walk(directory):
        for dirname in dirnames:
            folder_path = os.path.join(dirpath, dirname)
            if os.path.exists(folder_path):
                folders.append((folder_path, os.path.getmtime(folder_path)))

    folders.sort(key=lambda x: x[1])  # sort by modification time

    current_size_gb = get_directory_size(directory)
    deleted_folders_count = 0

    while current_size_gb > max_size_gb and folders:
        oldest_folder = folders.pop(0)
        try:
            shutil.rmtree(oldest_folder[0])
            # print(f"Deleted {oldest_folder[0]} to free up space.")
            deleted_folders_count += 1

        except Exception as e:
            print(f"Error deleting file {oldest_folder[0]}: {e}")
        current_size_gb = get_directory_size(directory)

    # check current storage usage
    # current_storage_gb = get_directory_size(RECEIVED_FILES_DIR)
    # print(f"Storage capacity {current_storage_gb}GB/{MAX_STORAGE_CAPACITY_GB}GB")
    # if current_storage_gb > MAX_STORAGE_CAPACITY_GB:
    #     clean_directory(RECEIVED_FILES_DIR, MAX_STORAGE_CAPACITY_GB)
