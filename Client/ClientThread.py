import threading


def run_on_separate_thread(func, *args, **kwargs):
    result = []
    print(f"Running function {func} with args: {args}, kwargs:{kwargs}")

    def wrapper():
        result.append(func(*args, **kwargs))

    thread = threading.Thread(target=wrapper)
    thread.start()
    thread.join()

    return result[0]
