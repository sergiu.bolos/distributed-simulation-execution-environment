import datetime
import getpass
import os.path
import requests
from SimulationUtilities import determine_task_size
from ClientThread import run_on_separate_thread

SERVERS = [
    {'name': 'Server 1', 'url': 'http://127.0.0.1:5001'},
    # {'name': 'Server 2', 'url': 'http://127.0.0.1:5002'}
]
TASKS_URL = '/tasks'
TASK_URL = '/task'
FILES_URL = '/files'
STATUS_URL = '/status'


class ConnectionHandler:
    def __init__(self):
        self.servers = SERVERS
        self.task_server_indexer = {}
        run_on_separate_thread(self.populate_indexer)

    def populate_indexer(self):
        user_id = getpass.getuser()

        for server in self.servers:
            try:
                response = requests.get(f"{server['url']}{TASKS_URL}", params={'user_id': user_id})
                response.raise_for_status()
                task_queue_data = response.json()['task_queue']

                # add tasks to the indexer with server name
                for task_id in task_queue_data:
                    self.task_server_indexer[task_id] = server['name']

            except requests.RequestException as e:
                print(f"TASK INDEXER: {server['name']}: {e}")

    # method to get all the task IDs from the indexes
    def get_task_indexer_keys(self):
        online_servers = [server['url'] for server in self.servers
                          if run_on_separate_thread(self.is_server_on, server['url'])]

        if not online_servers:
            print("No servers are currently online.")
            return []

        # return tasks from online servers
        online_tasks = [task_id for task_id, server_url in self.task_server_indexer.items() if
                        server_url in online_servers]
        return online_tasks

    @staticmethod
    def is_server_on(server_url):
        try:
            # access any (existing) endpoint to ensure that server is on
            response = requests.get(server_url + STATUS_URL)
            response.raise_for_status()
            print(f"Server {server_url} is ON")
            return response.status_code == 200
        except requests.RequestException:
            print(f"Server {server_url} is OFF")
            return False

    def decide_server(self):
        server_task_info = self.get_task_queue()
        print(f"Server Task Information: {server_task_info}")

        # check if either server is busy
        for server in self.servers:
            server_info = server_task_info.get(server['name'], {})
            if not server_info.get('busy', True):
                print(f"Selected server (not busy): {server['url']}")
                return server['url']

        # if both servers are busy, proceed with the weighted count logic
        sorted_servers = sorted(self.servers,
                                key=lambda x: server_task_info.get(x['name'], {'weighted_count': float('inf')}).get(
                                    'weighted_count', float('inf')))

        selected_server = sorted_servers[0]['url']
        print(f"Selected server (based on weighted count): {selected_server}")
        return selected_server

    def get_task_queue(self):
        server_info = {}
        try:
            for server in self.servers:
                if not self.is_server_on(server['url']):
                    print(f"Server {server['name']} is off.")
                    continue

                # make a request for retrieving the task queue
                response = requests.get(server['url'] + TASKS_URL)
                response.raise_for_status()
                task_queue_data = response.json()['task_queue']

                # initialize counters for task size
                task_counts = {'S': 0, 'M': 0, 'L': 0}

                # count the tasks by size
                for task in task_queue_data:
                    size = task['size']
                    if size in task_counts:
                        task_counts[size] += 1

                # check if the server is busy
                busy = any(task_counts.values())

                # compute weighted count
                weighted_count = task_counts['S'] + 2 * task_counts['M'] + 4 * task_counts['L']
                server_info[server['name']] = {'task_counts': task_counts,
                                               'busy': busy,
                                               'weighted_count': weighted_count}
                # server_info[server['name']] = {'task_counts': task_counts, 'busy': busy}
            return server_info

        except requests.RequestException as e:
            print(f'Error getting task queue: {e}')

    def add_task(self, file_path, tool_version):
        try:
            # specify task data
            task_data = {
                'user': getpass.getuser(),
                'data_path': 'path/to/data.zip',
                'result_path': 'path/to/result.rdb',
                'version': tool_version,
                'size': run_on_separate_thread(determine_task_size, file_path),
                'status': 'new',
                'start_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'end_time': 'TBA',
                'server': self.decide_server()
            }

            # open the file
            file_name = os.path.basename(file_path)
            files = {'file': (file_name, open(file_path, 'rb'))}

            # combine task data and file in a single request
            response = run_on_separate_thread(requests.post,
                                              task_data['server'] + TASKS_URL, data=task_data, files=files)
            response.raise_for_status()

            # check if response was successful
            if response.status_code == 201 and 'task_id' in response.json():
                task_id = response.json()['task_id']
                # Store server name instead of URL
                for server in self.servers:
                    if server['url'] == task_data['server']:
                        self.task_server_indexer[task_id] = server['name']
                        break
                return task_id, task_data, response.json()['message']

            return None, None

        except requests.RequestException as e:
            print(f'Error adding task: {e}')

    def get_task_status(self, task_id):
        try:
            # fetch server name from task id
            server_name = self.task_server_indexer.get(task_id)
            if not server_name:
                raise ValueError(f"No server found for task ID {task_id}")

            # get server URL by name
            server_url = next((server['url'] for server in self.servers if server['name'] == server_name), None)
            if not server_url:
                raise ValueError(f"No URL found for server {server_name}")

            response = requests.get(server_url + TASK_URL, params={'task_id': task_id})
            response.raise_for_status()
            task_info = response.json()
            return task_info

        except requests.RequestException as e:
            print(f'Error getting task status: {e}')
            return None

    def download_file(self, task_id, save_path):
        save_file_path = save_path
        try:
            # CLI case
            if os.path.isdir(save_path):
                save_file_path = os.path.join(save_path, f"{task_id}.zip")

            server_name = self.task_server_indexer.get(task_id)
            if not server_name:
                raise ValueError(f"No server found for task ID {task_id}")

            # get server URL by name
            server_url = next((server['url'] for server in self.servers if server['name'] == server_name), None)
            if not server_url:
                raise ValueError(f"No URL found for server {server_name}")

            response = requests.get(server_url + FILES_URL, params={'task_id': task_id})
            response.raise_for_status()
            with open(save_file_path, 'wb') as f:
                f.write(response.content)
            return True

        except requests.RequestException as e:
            print(f'Error downloading results: {e}')
            return False
