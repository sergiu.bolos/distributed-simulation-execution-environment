import argparse
import getpass
import os.path
import sys
import zipfile

from PyQt5.QtWidgets import QApplication

import ArtifactoryUtilities
from ConnectionHandler import ConnectionHandler
from dialogs.UserInterface import UserInterface

parser = argparse.ArgumentParser(description="CLI for interacting with Task Forge")
parser.add_argument('--add_task', action='store_true', help='Add a task')
parser.add_argument('--data_path', type=str, help='Absolute path to the zip file')
parser.add_argument('--version', type=str, help='Version of the tool')

parser.add_argument('--status', action='store_true', help='Get task status')
parser.add_argument('--status_task_id', type=str, help='ID of the task to check status')

parser.add_argument('--download', action='store_true', help='Download a task result')
parser.add_argument('--download_task_id', type=str, help='ID of the task to download')
parser.add_argument('--save_path', type=str, help='Path to directory with results')

if __name__ == '__main__':
    if len(sys.argv) > 1:
        # create connection handler instance
        connection_handler = ConnectionHandler()
        connection_handler.populate_indexer()

        print(f"Hello {getpass.getuser()}, welcome to the Task Forge.")
        if connection_handler.get_task_indexer_keys():
            print("Below is a list of your tasks.")
            print(connection_handler.get_task_indexer_keys())
        else:
            print("You currently have no tasks assigned to your user.")

        args = parser.parse_args(sys.argv[1:])

        if args.add_task:
            if args.data_path and args.version:
                if os.path.exists(args.data_path) and zipfile.is_zipfile(args.data_path):
                    if ArtifactoryUtilities.is_valid_version(args.version):
                            print("Adding task with data_path:", args.data_path, "and version:", args.version)
                            task_id = connection_handler.add_task(args.data_path, args.version)
                            if task_id:
                                print("Task added successfully. Task ID:", task_id)
                    else:
                        print(f"Version {args.version} is not valid.")
                else:
                    print("Error: the provided data path does not exist or is not a zip file.")
            else:
                print('Error: Both data_path and version must be specified for adding a task.')

        elif args.status:
            if args.status_task_id:
                print("Getting status for task with ID:", args.status_task_id)
                status_response = connection_handler.get_task_status(args.status_task_id)
                print(f"Task {args.status_task_id} is {status_response['status']}")
            else:
                print('Error: Task ID must be specified for checking task status.')

        elif args.download:
            if args.download_task_id and args.save_path:
                connection_handler.download_file(args.download_task_id, args.save_path)
                print(f"Downloaded results successfully at: {args.save_path}")
            else:
                print('Error: Task ID and the save path must be specified for downloading a task result.')

    else:
        app = QApplication(sys.argv)
        main = UserInterface(app)
        app.setStyle('Fusion')
        main.setWindowTitle("Task Forge")
        main.exec_()
