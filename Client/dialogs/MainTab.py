import datetime
import os
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QPalette, QColor
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QComboBox, QPushButton, QLabel, QFileDialog, \
    QLineEdit, QTableWidget, QTableWidgetItem
from pyqt_switch import PyQtSwitch
from ConnectionHandler import ConnectionHandler
from ArtifactoryUtilities import get_tool_versions
from ClientThread import run_on_separate_thread


class MainTab(QWidget):
    def __init__(self, user_interface):
        super(MainTab, self).__init__()
        # pass instance of user_interface to access common elements
        self.user_interface = user_interface

        # create connection handler instance
        self.connection_handler = ConnectionHandler()
        # self.connection_handler.populate_indexer()

        # create main layout
        main_layout = QVBoxLayout()

        # initialize icons
        current_dir = os.path.dirname(os.path.abspath(__file__))
        self.moon_pixmap = QPixmap(os.path.join(current_dir, "../img/moon.png"))
        self.sun_pixmap = QPixmap(os.path.join(current_dir, "../img/sun.png"))

        # dark mode switch
        self.toggle_icon = QLabel()
        self.toggle_icon.setPixmap(self.moon_pixmap.scaled(20, 20))
        self.toggle_button = PyQtSwitch()
        self.toggle_button.toggled.connect(self.__toggled)
        self.toggle_button.setAnimation(True)

        version_label = QLabel('STools Version:')
        self.cbox_version = QComboBox()
        self.populate_cbox()

        version_layout = QHBoxLayout()
        version_layout.addWidget(version_label)
        version_layout.addWidget(self.cbox_version)
        version_layout.addStretch(1)
        version_layout.addWidget(self.toggle_icon)
        version_layout.addWidget(self.toggle_button)

        # data management section
        data_label = QLabel('File path:')
        self.data_path = QLineEdit()
        button_select = QPushButton('Select File')
        # noinspection PyUnresolvedReferences
        button_select.clicked.connect(self.select_data)

        data_layout = QHBoxLayout()
        data_layout.addWidget(data_label)
        data_layout.addWidget(self.data_path)
        data_layout.addWidget(button_select)

        # task table section
        table_main_layout = QVBoxLayout()

        self.tableWidget = QTableWidget()
        self.tableWidget.setColumnCount(7)

        headers = ["Task ID", "Size", "Status", "Start", "End", "Server", "Actions"]
        self.tableWidget.setHorizontalHeaderLabels(headers)

        # create refresh button
        button_refresh = QPushButton('Refresh Tasks')
        # noinspection PyUnresolvedReferences
        button_refresh.clicked.connect(self.refresh_clicked)

        refresh_layout = QHBoxLayout()
        refresh_layout.addWidget(button_refresh)
        refresh_layout.addStretch(1)

        table_main_layout.addWidget(self.tableWidget)
        table_main_layout.addLayout(refresh_layout)

        # connect layouts to the main one
        main_layout.addLayout(version_layout)
        main_layout.addLayout(data_layout)
        main_layout.addLayout(table_main_layout)
        main_layout.addStretch(1)
        self.setLayout(main_layout)
        self.fill_table_with_user_tasks()

    def get_task_queue(self):
        server_info = self.connection_handler.get_task_queue()

        if server_info:
            # output the result
            for server_name, info in server_info.items():
                no_of_tasks = info['task_counts']
                busy = info['busy']
                msg = f"{server_name}: {no_of_tasks:}, {'Busy' if busy else 'Available'}"
                self.log_message(msg)
        else:
            self.log_message('Failed to establish connection.')

    # method for uploading data to GUI
    def select_data(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.ExistingFile)
        file_dialog.setNameFilter("")
        if file_dialog.exec_():
            file_paths = file_dialog.selectedFiles()
            self.data_path.setText(file_paths[0])
        else:
            self.log_message('File selection canceled.')

    # method for adding a task to the server
    def add_task(self):
        data_path_text = self.data_path.text()
        if not data_path_text:
            self.log_message('Please select the data before sending the task.')
            return

        # get tool version:
        selected_version = self.cbox_version.currentText().strip()

        task_id, task_data, upload_response = self.connection_handler.add_task(data_path_text, selected_version)
        if task_id:
            self.log_message(upload_response)

            # pass information to the table
            task_info = {
                'task_id': task_id,
                'size': task_data['size'],
                'status': task_data['status'],
                'start': task_data['start_time'],
                'end': task_data['end_time'],
                'server': self.connection_handler.task_server_indexer[task_id]  # changed line
            }
            self.update_task_table(task_info)

    def update_task_table(self, task_info):
        row_position = self.tableWidget.rowCount()
        self.tableWidget.insertRow(row_position)
        for col, (key, value) in enumerate(task_info.items()):
            item = QTableWidgetItem(str(value))
            item.setFlags(item.flags() ^ Qt.ItemIsEditable)
            item.setTextAlignment(Qt.AlignCenter)
            self.tableWidget.setItem(row_position, col, item)

        # add button for downloading
        download_button = QPushButton("Download")
        # noinspection PyUnresolvedReferences
        download_button.clicked.connect(self.download_clicked)
        self.tableWidget.setCellWidget(row_position, len(task_info), download_button)

    # method used for updating the status of the task upon refreshing
    def update_task_attribute(self, row, column, value):
        item = self.tableWidget.item(row, column)
        if item:
            item.setText(str(value))
            item.setTextAlignment(Qt.AlignCenter)
        else:
            item = QTableWidgetItem(str(value))
            item.setTextAlignment(Qt.AlignCenter)
            self.tableWidget.setItem(row, column, item)

    def refresh_clicked(self):
        id_list = self.get_task_ids()

        for row, task_id in enumerate(id_list):
            task_info = run_on_separate_thread(self.connection_handler.get_task_status, task_id)
            if task_info:
                self.update_task_attribute(row, 2, task_info.get('status'))
                self.update_task_attribute(row, 4, task_info.get('end_time'))

    def download_clicked(self):
        button = self.sender()
        index = self.tableWidget.indexAt(button.pos())

        id_item = self.tableWidget.item(index.row(), 0)
        if id_item is not None:
            task_id = id_item.text()
            file_path, _ = QFileDialog.getSaveFileName(self, "Save File", "", "ZIP Files (*.zip)")

            if not file_path:
                self.log_message('Save location selection canceled.')

            if run_on_separate_thread(self.connection_handler.download_file, task_id, file_path):
                self.log_message(f'Results downloaded successfully at: {file_path}')
            else:
                self.log_message(f'Error downloading results.')

    # function that retrieves the tasks ID from a table
    def get_task_ids(self):
        task_id_list = []

        # get number of rows in the table
        num_rows = self.tableWidget.rowCount()

        for row in range(num_rows):
            item = self.tableWidget.item(row, 0)
            if item is not None:
                task_id = item.text()
                task_id_list.append(task_id)

        return task_id_list

    def fill_table_with_user_tasks(self):
        user_tasks = self.connection_handler.get_task_indexer_keys()
        for task_id in user_tasks:
            task_info = self.connection_handler.get_task_status(task_id)

            if task_info:
                task_info['task_id'] = task_id
                task_info['server'] = self.connection_handler.task_server_indexer.get(task_id)  # changed line
                formatted_task_info = {
                    'Task ID': task_info.get('task_id'),
                    'Size': task_info.get('size'),
                    'Status': task_info.get('status'),
                    'Start': task_info.get('start_time'),
                    'End': task_info.get('end_time'),
                    'Server': task_info.get('server')
                }
                self.update_task_table(formatted_task_info)

    def set_dark_mode(self, main_app):
        dark_palette = QPalette()
        dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.WindowText, Qt.white)
        dark_palette.setColor(QPalette.Base, QColor(35, 35, 35))
        dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ToolTipBase, QColor(25, 25, 25))
        dark_palette.setColor(QPalette.ToolTipText, Qt.white)
        dark_palette.setColor(QPalette.Text, Qt.white)
        dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.ButtonText, Qt.white)
        dark_palette.setColor(QPalette.BrightText, Qt.red)
        dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        dark_palette.setColor(QPalette.HighlightedText, QColor(35, 35, 35))
        dark_palette.setColor(QPalette.Active, QPalette.Button, QColor(53, 53, 53))
        dark_palette.setColor(QPalette.Disabled, QPalette.ButtonText, Qt.darkGray)
        dark_palette.setColor(QPalette.Disabled, QPalette.WindowText, Qt.darkGray)
        dark_palette.setColor(QPalette.Disabled, QPalette.Text, Qt.darkGray)
        dark_palette.setColor(QPalette.Disabled, QPalette.Light, QColor(53, 53, 53))
        self.toggle_icon.setPixmap(self.sun_pixmap.scaled(25, 25))
        main_app.setPalette(dark_palette)

    def set_light_mode(self, main_app):
        self.toggle_icon.setPixmap(self.moon_pixmap.scaled(20, 20))
        main_app.setPalette(self.style().standardPalette())

    def __toggled(self, f):
        if f:
            self.set_dark_mode(self.user_interface)
        else:
            self.set_light_mode(self.user_interface)

    def populate_cbox(self):
        versions = run_on_separate_thread(get_tool_versions)
        for version in versions:
            self.cbox_version.addItem(version)
        self.cbox_version.setCurrentIndex(self.cbox_version.count() - 1)  # set version to latest by default

    def log_message(self, message):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.user_interface.text_logger.append(f"[{timestamp}] {message}")
