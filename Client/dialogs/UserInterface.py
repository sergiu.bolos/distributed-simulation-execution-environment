import webbrowser

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QDialog, QPushButton, QHBoxLayout, QTextEdit, QMenuBar, \
    QAction

from dialogs.MainTab import MainTab


class UserInterface(QDialog):
    def __init__(self, main_app):
        super().__init__()
        self.setWindowFlags(Qt.Window)
        self.app = main_app
        self.resize(1050, 600)
        self.setWindowTitle('Simulation Server')
        self.setWindowFlag(Qt.Window)

        # create layout
        main_layout = QVBoxLayout()
        self.create_menu_bar(main_layout)

        # create main window
        self.main_window = MainTab(self)

        main_layout.addWidget(self.main_window)

        self.text_logger = QTextEdit()
        self.text_logger.setReadOnly(True)
        self.text_logger.setLineWrapMode(QTextEdit.WidgetWidth)

        log_layout = QVBoxLayout()
        log_layout.addWidget(self.text_logger)

        button_run = QPushButton('Run')
        # noinspection PyUnresolvedReferences
        button_run.clicked.connect(self.main_window.add_task)

        button_get_queue = QPushButton('Check Servers')
        # noinspection PyUnresolvedReferences
        button_get_queue.clicked.connect(self.main_window.get_task_queue)

        button_clear = QPushButton('Clear Log')
        # noinspection PyUnresolvedReferences
        button_clear.clicked.connect(self.clear_logger)

        button_close = QPushButton('Close')
        # noinspection PyUnresolvedReferences
        button_close.clicked.connect(self.close)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(button_run)
        buttons_layout.addWidget(button_get_queue)
        buttons_layout.addWidget(button_clear)
        buttons_layout.addStretch(1)
        buttons_layout.addWidget(button_close)

        # add table layout
        main_layout.addStretch(1)
        main_layout.addLayout(log_layout)
        main_layout.addLayout(buttons_layout)
        self.setLayout(main_layout)

    def create_menu_bar(self, main_layout):
        menu_bar = QMenuBar(self)

        about_action = QAction('User Guide', self)
        about_action.setStatusTip('About Task Forge...')
        # noinspection PyUnresolvedReferences
        about_action.triggered.connect(self.on_about)

        help_menu = menu_bar.addMenu("&Help")
        help_menu.addAction(about_action)

        menu_layout = QHBoxLayout()
        menu_layout.addWidget(menu_bar)

        main_layout.addLayout(menu_layout)

    @staticmethod
    def on_about():
        url = "https://github.geo.conti.de/uif74635/ReSimServer"
        webbrowser.open(url, new=0, autoraise=True)

    def clear_logger(self):
        self.text_logger.clear()
