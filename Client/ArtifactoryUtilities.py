from artifactory import ArtifactoryPath

ARTIFACTORY_PATH = ArtifactoryPath("https://eu.artifactory.conti.de/artifactory/pss_algo_tools_generic_l/bin/tools"
                                   "/stools/releases")


# returns a list of tool versions
def get_tool_versions():
    ver_list = []
    for path in ARTIFACTORY_PATH:
        path_list = str(path).split("/")
        ver_list.append(path_list[-1])

    return ver_list


def is_valid_version(version):
    tool_versions = get_tool_versions()
    return version in tool_versions
