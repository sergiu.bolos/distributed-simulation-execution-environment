import os
import re
import shutil
import uuid
import zipfile


def determine_task_size(data_path):
    # first, obtain the crash count
    crash_count = 0
    crash_regex = re.compile(r'^\* \d+')
    ctl_regex = re.compile('(.*ctl$)')
    temp_dir = os.path.join(os.path.dirname(data_path), f"{str(uuid.uuid4())}")

    # create temp dir if not exists
    os.makedirs(temp_dir, exist_ok=True)

    try:
        with zipfile.ZipFile(data_path, 'r') as zip_ref:
            zip_ref.extractall(temp_dir)

        for root, _, files in os.walk(temp_dir):
            for file in files:
                if ctl_regex.match(file):
                    # open file and count crashes
                    path_to_ctl = os.path.join(root, file)

                    with open(path_to_ctl, 'r') as ctl_file:
                        lines = ctl_file.readlines()

                    for line in lines:
                        if crash_regex.match(line):
                            crash_count += 1
    finally:
        shutil.rmtree(temp_dir)

    # then, determine the task size based on the crash count
    if crash_count < 2:
        return 'S'
    elif 2 < crash_count < 5:
        return 'M'
    else:
        return 'L'
